package main

import (
	"encoding/json"
	"log"
	"syscall/js"
)

type Human struct {
	FirstName string `json:"firstName"`
	LastName  string `json:"lastName"`
}

func GiveMeHumanJsonObject(this js.Value, args []js.Value) interface{} {

	jsonHuman, err := json.Marshal(
		Human{
			args[0].String(),
			args[1].String()})

	if err != nil {
		log.Fatalf(
			"Error occured during marshaling: %s",
			err.Error())
	}

	JSON := js.Global().Get("JSON")
	jsonString := string(jsonHuman)

	return JSON.Call("parse", jsonString)

}

func GiveMeAnotherHumanJsonObject(this js.Value, args []js.Value) interface{} {

	human := args[0]

	firstName := human.Get("firstName").String()
	lastName := human.Get("lastName").String()

	jsonHuman, err := json.Marshal(Human{firstName, lastName})

	if err != nil {
		log.Fatalf(
			"Error occured during marshaling: %s",
			err.Error())
	}

	JSON := js.Global().Get("JSON")
	jsonString := string(jsonHuman)

	return JSON.Call("parse", jsonString)

}

func main() {

	js.Global().Set(
		"GiveMeHumanJsonObject",
		js.FuncOf(GiveMeHumanJsonObject))

	js.Global().Set(
		"GiveMeAnotherHumanJsonObject",
		js.FuncOf(GiveMeAnotherHumanJsonObject))

	<-make(chan bool)
}