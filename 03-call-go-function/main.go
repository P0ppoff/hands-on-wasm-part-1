package main

import (
	"syscall/js"
)

func Hello(this js.Value, args []js.Value) interface{} {
	message := args[0].String()
	return "😃 Hello " + message
}

func Hey(this js.Value, args []js.Value) interface{} {
	return "👋🏼 Hey " + args[0].String()
}

func main() {
	js.Global().Set("Hello", js.FuncOf(Hello))
	js.Global().Set("Hey", js.FuncOf(Hey))

	<-make(chan bool)
}